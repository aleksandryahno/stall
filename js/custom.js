"use strict";

console.log('stylonia');

$(document).ready(function() {

    // burger
    $("#burgerBtn").click(function () {

        $(this).toggleClass("burger-active");
        $(".menu-hide").toggleClass('menu-visible');

    });

    // On press "Esc button"
    $(document).keyup(function(e) {

        if (e.keyCode === 27) {
            $("#burgerBtn").removeClass('burger-active');
            $(".menu-hide").removeClass('menu-visible');
        }

    });

    // filter
    $( ".bnt-filter1" ).click(function() {
        $( ".box-filter1" ).slideToggle( "slow" );
    });
    $( ".bnt-filter2" ).click(function() {
        $( ".box-filter2" ).slideToggle( "slow" );
    });
    $( ".bnt-filter3" ).click(function() {
        $( ".box-filter3" ).slideToggle( "slow" );
    });
    $( ".bnt-filter4" ).click(function() {
        $( ".box-filter4" ).slideToggle( "slow" );
    });
    $( ".bnt-filter5" ).click(function() {
        $( ".box-filter5" ).slideToggle( "slow" );
    });
    $( ".bnt-filter6" ).click(function() {
        $( ".box-filter6" ).slideToggle( "slow" );
    });
    $( ".bnt-filter7" ).click(function() {
        $( ".box-filter7" ).slideToggle( "slow" );
    });

    // slick `card`
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        dots: false,
        centerMode: true,
        focusOnSelect: true
    });


});



